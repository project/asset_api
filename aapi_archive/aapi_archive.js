function aapi_archive_browser_thumb(){
  var embed = '';
  $('#asset-view').html(embed);
  aapi_archive_browser_tools();
}

function aapi_archive_browser_tools(){
  aapi_inclusion_field('');
  $('#asset-inclusion').change(function(){
  });
}

//----------------------------------------------------------------------------//

function aapi_archive_content_caption_attach() {
  var captions = $('table.asset-archive-content caption');
  captions.css('cursor', 'pointer').css('width', '100%').click(function(e){
      if ($(this).next('thead').css('display') == 'none') {
        $(this).next('thead').css('display', '').next('tbody').css('display', '');
      }
      else {
        $(this).next('thead').css('display', 'none').next('tbody').css('display', 'none');
      }
  });
  captions.next('thead').css('display', 'none').next('tbody').css('display', 'none');
}
$(document).ready(aapi_archive_content_caption_attach);