function aapi_doc_browser_thumb(){
  var info = '';
  aapi_doc_browser_tools(info);
}

function aapi_doc_browser_tools(info){
  aapi_inclusion_field(info);
  $('#asset-inclusion').change(function(){
    $.ajax({
      type: "GET",
      url: Drupal.settings.aapi.callback+'/'+$(this).val(),
      dataType: "json",
      success: function(msg){
        $('#asset-view').html(msg);
      }
    });
  });
}