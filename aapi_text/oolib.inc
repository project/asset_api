<?php

/**
 * @file
 * Allows to deal with openoffice files
 * 
 * @todo: caching functionalities and performances improvements
 */


/**
 * Extract data from the openoffice file
 *
 * @param object $node
 * @param boolean $override
 * @return string
 */
function aapi_text_oolib_unpack($node, $override=FALSE) {
  if (aapi_include('pclzip')) {
    $path = dirname($node->filepath) .'/'. basename($node->filepath, '.'. $node->extension) .'_content';
    $path = dirname($node->filepath) .'/'. str_replace('.', '_', basename($node->filepath)) .'_content';
    
    if (is_dir($path) && $override!=TRUE) {
      return $path;
    }
    drupal_set_message('unpacking: '. $node->filepath);
    
    
    $archive = new PclZip($node->filepath);
    if ($archive->extract(PCLZIP_OPT_PATH, $path) == 0) {
      drupal_set_message($archive->errorInfo(true));
      return FALSE;
    }
    
    return $path;
  }
  return FALSE;
}

/**
 * Read an open office file
 *
 * @param object $node
 * @return string
 */
function aapi_text_oolib_read($node) {
  // take a look at /usr/lib/openoffice/share/
  if ($path = aapi_text_oolib_unpack($node)) {
    $content_path = $path .'/content.xml';
    $xmldata = file_get_contents($content_path);
    $content = strip_tags($xmldata);
    
    
    // DONE: merge xslt-php4-to-php5.php with this file (as authorised by the owner - http://alexandre.alapetite.net/blog/notes/xslt-php4-php5.html)
    //include_once(drupal_get_path('module', 'aapi_text') .'/xslt-php4-to-php5.php');
    $arguments = array('/_xml' => $xmldata);
    $params = array(
      'targetBaseURL' => dirname(__FILE__) .'/',
      'targetURL' => dirname(__FILE__) .'/',
      'sourceBaseURL' => dirname(__FILE__) .'/',
      //'targetURL' => dirname(__FILE__) .'/',
      //'java' => 'false()',
    );
    $xsltproc = xslt_create();
    xslt_set_encoding($xsltproc, 'ISO-8859-1');
    $html =
        xslt_process($xsltproc, 'arg:/_xml', drupal_get_path('module', 'aapi_text') ."/misc_xsl/mod_bitflux_odt2html.xsl", NULL, $arguments);//, $params);

    xslt_free($xsltproc);
    $content = $html;
    
  }
  else {
    $content = t('The content of !filename can not be read .', array('!filename' => $node->filename));
  }
  return $content;
}

/**
 * Extract the thumbnail of an openoffice file
 *
 * @param object $node
 * @return string
 */
function aapi_text_oolib_thumb($node) {
  if ($path = aapi_text_oolib_unpack($node)) {
    $filepath = $node->filepath;
    $thumb_path = $path .'/Thumbnails/thumbnail.png';
    
    if (!file_exists($thumb_path)) {
      $thumb_path = drupal_get_path('module', 'aapi') .'/img/emblem-unreadable.png';
    }
  }
  else {
    $thumb_path = drupal_get_path('module', 'aapi') .'/img/emblem-unreadable.png';
  }
  return $thumb_path;
}




/*
 Nécessite PHP5, utilise l'extension XSL (à activer)
 Pour être utilisé dans des scripts PHP4 utilisant l'extension XSLT (à activer)
 Permet aux scripts PHP4/XSLT de fonctionner avec PHP5/XSL
 http://alexandre.alapetite.net/doc-alex/xslt-php4-php5/
*/

$xslArgs=null;
function xslt_create() {return new xsltprocessor();}
function xslt_errno($xh) {return 7;}
function xslt_error($xh) {return '?';}
function xslt_free($xh) {unset($xh);}
function xslt_process($xh,$xmlcontainer,$xslcontainer,$resultcontainer=null,$arguments=array(),$parameters=array())
{//Voir aussi : http://alexandre.alapetite.net/doc-alex/domxml-php4-php5/
 //Basé sur : http://www.php.net/manual/ref.xsl.php#45415
 $xml=new DOMDocument();
 $basedir=$xh->getParameter('sablotron','xslt_base_dir');
 if ($basedir && ($workdir=getcwd()))
  chdir($basedir);
 if (substr($xmlcontainer,0,4)=='arg:')
  $xml->loadXML($arguments[substr($xmlcontainer,4)]);
 else $xml->load($xmlcontainer);
 $xsl=new DOMDocument();
 if (substr($xslcontainer,0,4)=='arg:')
  $xsl_=&$arguments[substr($xslcontainer,4)];
 else $xsl_=file_get_contents($xslcontainer);
 $xsl->loadXML(str_replace('arg:/','arg://',$xsl_));
 $xh->importStyleSheet($xsl);
 global $xslArgs;
 $xslArgs=$arguments;
 foreach ($parameters as $param=>$value)
  $xh->setParameter('',$param,$value);
 $result=$xh->transformToXML($xml);
 if (isset($resultcontainer))
  file_put_contents($resultcontainer,$result); 
 if ($basedir && $workdir)
  chdir($workdir);
 if (isset($resultcontainer))
  return true;
 else return $result;
}
function xslt_set_base($xh,$base) {$xh->setParameter('sablotron','xslt_base_dir',str_replace('file://','',$base));}
function xslt_set_encoding($xh,$encoding) {} //Encodage manuel, ou utiliser xsl:output @encoding dans le document XSL
function xslt_set_error_handler($xh,$handler) {}

class xslt_arg_stream
{
 public $position;
 private $xslArg;
 function stream_eof() {return $this->position>=strlen($this->xslArg);}
 function stream_open($path,$mode,$options,&$opened_path)
 {
  $this->position=0;
  $url=parse_url($path);
  $varname=$url['host'];
  global $xslArgs;
  if (isset($xslArgs['/'.$varname]))
   $this->xslArg=&$xslArgs['/'.$varname];
  elseif (isset($xslArgs[$varname]))
   $this->xslArg=&$xslArgs[$varname];
  else return false;
  return true;
 }
 function stream_read($count)
 {
  $ret=substr($this->xslArg,$this->position,$count);
  $this->position+=strlen($ret);
  return $ret;
 }
 function stream_tell() {return $this->position;}
 function url_stat() {return array();}
}

stream_wrapper_register('arg','xslt_arg_stream');
