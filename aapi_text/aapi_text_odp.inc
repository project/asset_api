<?php

if (function_exists('drupal_get_path') || isset($aapi_text_include_path)) {
  $aapi_text_include_path = @drupal_get_path('module', 'aapi_text');
}
else {
  $aapi_text_include_path = dirname(__FILE__);
}
include_once($aapi_text_include_path .'/oolib.inc');

function aapi_text_odp_unpack($node, $override=FALSE) {
  return aapi_text_oolib_unpack($node, $override);
}

function aapi_text_odp_read($node) {
  return '<div class="asset-content '. $node->extension .'-xml-content">'. aapi_text_oolib_read($node) .'</div>';
}

function aapi_text_odp_thumb($node) {
  return aapi_text_oolib_thumb($node);
}

