/**
 *
 */

/* Import plugin specific language pack */
tinyMCE.importPluginLanguagePack('asset');

var TinyMCE_AssetPlugin = {
  getInfo : function() {
    return {
      longname : 'Asset',
      author : 'Valentin zeropaper Vago',
      authorurl : 'http://irata.ch',
      infourl : 'http://irata.ch/tinymce_asset',
      version : tinyMCE.majorVersion + "." + tinyMCE.minorVersion
    };
  },

  initInstance : function(inst) {
    if (!tinyMCE.settings['asset_skip_plugin_css'])
      tinyMCE.importCSS(inst.getDoc(), tinyMCE.baseURL + "/plugins/asset/css/content.css");
  },

  getControlHTML : function(cn) {
    switch (cn) {
      case "asset":
        return tinyMCE.getButtonHTML(cn, 'lang_asset_desc', '{$pluginurl}/images/asset.gif', 'mceAsset');
    }

    return "";
  },

  execCommand : function(editor_id, element, command, user_interface, value) {
    // Handle commands
    switch (command) {
      case "mceAsset":
        tinyMCE.openWindow({
            file : aapiUrl('asset-browser/popup', 'use=tinymce'),
            width : 800,
            height : 600,
            title : 'Asset browser'
          }, {
            editor_id : editor_id,
            inline : "yes",
            resizable : "yes"
        });
        
        return true;
     }

     // Pass to next handler in chain
     return false;
  },

  cleanup : function(type, content, inst) {
    var nl, img, i, ne, d, s, ci;

    switch (type) {
      case "insert_to_editor":
        img = tinyMCE.getParam("theme_href") + '/images/spacer.gif';
        var matches = /<asset([^>]*)>*<\/asset>/gi.exec(content);
        content = content.replace(/<asset([^>]*)>*<\/asset>/gi, '<img class="mceItemAsset asset" $1 src="' + img + '" />');
        break;

      case "insert_to_editor_dom":
        d = inst.getDoc();
//        nl = content.getElementsByTagName("img");
        break;

      case "get_from_editor":
        var startPos = -1, endPos, attribs, chunkBefore, chunkAfter, embedHTML, at, pl, cb, mt, ex;

        while ((startPos = content.indexOf('<img', startPos+1)) != -1) {
          endPos = content.indexOf('/>', startPos);
          attribs = TinyMCE_AssetPlugin._parseAttributes(content.substring(startPos + 4, endPos));

          // Is not flash, skip it
          if (attribs.class != 'mceItemAsset asset')
            continue;

          endPos += 2;
          
          if (attribs.style == undefined)
            attribs.style = '';

          embedHTML = '<asset nid="'+ attribs.nid +'" width="'+ attribs.width +'" mode="'+ attribs.mode +'" height="'+ attribs.height +'" style="'+ attribs.style +'"></asset>';
          
          // Insert embed/object chunk
          chunkBefore = content.substring(0, startPos);
          chunkAfter = content.substring(endPos);
          content = chunkBefore + embedHTML + chunkAfter;
        }
        break;
    }

    return content;
  },

  handleNodeChange : function(editor_id, node, undo_index, undo_levels, visual_aid, any_selection) {
    if (node == null)
      return;

    do {
      if (node.nodeName == "IMG" && /mceItemAsset/.test(tinyMCE.getAttrib(node, 'class'))) {
        tinyMCE.switchClass(editor_id + '_asset', 'mceButtonSelected');
        return true;
      }
    } while ((node = node.parentNode));

    tinyMCE.switchClass(editor_id + '_asset', 'mceButtonNormal');

    return true;
  },

  _createImgFromEmbed : function(n, d, cl) {
    var ne, at, i, ti = '', an;

    ne = d.createElement('img');
    ne.src = tinyMCE.getParam("theme_href") + '/images/spacer.gif';
    aapiDbg('_createImgFromEmbed: '+ne.src);
    ne.width = tinyMCE.getAttrib(n, 'width');
    ne.height = tinyMCE.getAttrib(n, 'height');
    ne.mode = tinyMCE.getAttrib(n, 'mode');
    ne.nid = tinyMCE.getAttrib(n, 'nid');
    ne.className = cl;

    at = n.attributes;
    for (i=0; i<at.length; i++) {
      if (at[i].specified && at[i].nodeValue) {
        an = at[i].nodeName.toLowerCase();

        if (an == 'src')
          continue;

        if (an == 'mce_src')
          an = 'src';

        if (an.indexOf('mce_') == -1 && !new RegExp('^(class|type)$').test(an))
          ti += an.toLowerCase() + ':\'' + at[i].nodeValue + "',";
      }
    }

    ti = ti.length > 0 ? ti.substring(0, ti.length - 1) : ti;
    ne.title = ti;

    n.parentNode.replaceChild(ne, n);
  },

  _createImg : function(cl, d, n) {
    var i, nl, ti = "", an, av, al = new Array();

    ne = d.createElement('img');
    ne.src = tinyMCE.getParam("theme_href") + '/images/spacer.gif';
    aapiDbg('_createImg: '+ne.src);
    ne.width = tinyMCE.getAttrib(n, 'width');
    ne.height = tinyMCE.getAttrib(n, 'height');
    ne.className = cl;

    al.id = tinyMCE.getAttrib(n, 'id');
    al.nid = tinyMCE.getAttrib(n, 'nid');
    al.mode = tinyMCE.getAttrib(n, 'mode');
    al.name = tinyMCE.getAttrib(n, 'name');
    al.width = tinyMCE.getAttrib(n, 'width');
    al.height = tinyMCE.getAttrib(n, 'height');
    al.bgcolor = tinyMCE.getAttrib(n, 'bgcolor');
    al.align = tinyMCE.getAttrib(n, 'align');
    al.class_name = tinyMCE.getAttrib(n, 'mce_class');

    nl = n.getElementsByTagName('div');
    for (i=0; i<nl.length; i++) {
      av = tinyMCE.getAttrib(nl[i], 'value');
      av = av.replace(new RegExp('\\\\', 'g'), '\\\\');
      av = av.replace(new RegExp('"', 'g'), '\\"');
      av = av.replace(new RegExp("'", 'g'), "\\'");
      an = tinyMCE.getAttrib(nl[i], 'name');
      al[an] = av;
    }

    if (al.movie) {
      al.src = al.movie;
      al.movie = null;
    }

    for (an in al) {
      if (al[an] != null && typeof(al[an]) != "function" && al[an] != '')
        ti += an.toLowerCase() + ':\'' + al[an] + "',";
    }

    ti = ti.length > 0 ? ti.substring(0, ti.length - 1) : ti;
    ne.title = ti;

    return ne;
  },

//  _getEmbed : function(cls, cb, mt, p, at) {
//    var h = '', n;
//
//    p.width = at.width ? at.width : p.width;
//    p.height = at.height ? at.height : p.height;
//
//    h += '<object classid="clsid:' + cls + '" codebase="' + cb + '"';
//    h += typeof(p.id) != "undefined" ? ' id="' + p.id + '"' : '';
//    h += typeof(p.name) != "undefined" ? ' name="' + p.name + '"' : '';
//    h += typeof(p.width) != "undefined" ? ' width="' + p.width + '"' : '';
//    h += typeof(p.height) != "undefined" ? ' height="' + p.height + '"' : '';
//    h += typeof(p.align) != "undefined" ? ' align="' + p.align + '"' : '';
//    h += '>';
//
//    for (n in p) {
//      if (typeof(p[n]) != "undefined" && typeof(p[n]) != "function") {
//        h += '<param name="' + n + '" value="' + p[n] + '" />';
//
//        // Add extra url parameter if it's an absolute URL on WMP
//        if (n == 'src' && p[n].indexOf('://') != -1 && mt == 'application/x-mplayer2')
//          h += '<param name="url" value="' + p[n] + '" />';
//      }
//    }
//
//    h += '<embed type="' + mt + '"';
//
//    for (n in p) {
//      if (typeof(p[n]) == "function")
//        continue;
//
//      // Skip url parameter for embed tag on WMP
//      if (!(n == 'url' && mt == 'application/x-mplayer2'))
//        h += ' ' + n + '="' + p[n] + '"';
//    }
//
//    h += '></embed></object>';
//
//    return h;
//  },

  _parseAttributes : function(attribute_string) {
    var attributeName = "", endChr = '"';
    var attributeValue = "";
    var withInName;
    var withInValue;
//    var attributes = new Array();
    var attributes = {};
    var whiteSpaceRegExp = new RegExp('^[ \n\r\t]+', 'g');

    if (attribute_string == null || attribute_string.length < 2)
      return null;

    withInName = withInValue = false;

    for (var i=0; i<attribute_string.length; i++) {
      var chr = attribute_string.charAt(i);

      if ((chr == '"' || chr == "'") && !withInValue) {
        withInValue = true;
        endChr = chr;
      } else if (chr == endChr && withInValue) {
        withInValue = false;

        var pos = attributeName.lastIndexOf(' ');
        if (pos != -1)
          attributeName = attributeName.substring(pos+1);

        attributes[attributeName.toLowerCase()] = attributeValue.substring(1);

        attributeName = "";
        attributeValue = "";
      } else if (!whiteSpaceRegExp.test(chr) && !withInName && !withInValue)
        withInName = true;

      if (chr == '=' && withInName)
        withInName = false;

      if (withInName)
        attributeName += chr;

      if (withInValue)
        attributeValue += chr;
    }

    return attributes;
  }
};

tinyMCE.addPlugin("asset", TinyMCE_AssetPlugin);