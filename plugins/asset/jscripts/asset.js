var oldWidth, oldHeight, win, getBrowserHTML, url;

if (window.parent.parent.tinyMCE != undefined) {
  win = window.parent.parent;
}
else if (window.parent.tinyMCE != undefined) {
  win = window.parent;
}

if (win && win.tinyMCE) {
  var tinyMCE = win.tinyMCE;
  
  // @todo: ...is it possible to shoot that? 
  getBrowserHTML = win.getBrowserHTML;
  
  url = tinyMCE.getParam("asset_external_list_url");
}

function init() {
  if (tinyMCE) {
  	var pl = "", f, val;
  	var type = "flash", fe, i;
    
    buildForm();
    
    f = document.getElementById('asset-editor');
  
  	fe = tinyMCE.selectedInstance.getFocusElement();
    
    if (/mceItemAsset/.test(tinyMCE.getAttrib(fe, 'class'))) {
      $('#asset-editor-width').val(tinyMCE.getAttrib(fe, 'width'));
      $('#asset-editor-height').val(tinyMCE.getAttrib(fe, 'height'));
      $('#asset-editor-name').val(tinyMCE.getAttrib(fe, 'name'));
      $('#asset-editor-mode').val(tinyMCE.getAttrib(fe, 'mode'));
      $('#asset-editor-nid').val(tinyMCE.getAttrib(fe, 'nid'));
      
      if (tinyMCE.getAttrib(fe, 'style')) $('#asset-editor-style').val(tinyMCE.getAttrib(fe, 'style'));
      if (tinyMCE.getAttrib(fe, 'id')) $('#asset-editor-id').val(tinyMCE.getAttrib(fe, 'id'));
      
  		document.forms[0].insert.value = tinyMCE.getLang('lang_update', 'Insert', true);
      getPreview();
  	}
  }
}

function buildForm() {
  $('#asset-view').after('<form id="asset-editor">'
    + '<div class="form-item"><label for="asset-editor-width">'+ aapiT('width') +'</label>'
    + '<input id="asset-editor-width" type="text" name="width" /></div>'
    
    + '<div class="form-item"><label for="asset-editor-height">'+ aapiT('height') +'</label>'
    + '<input id="asset-editor-height" type="text" name="height" /></div>'
    
    + '<div class="form-item"><label for="asset-editor-mode">'+ aapiT('mode') +'</label>'
    + '<select id="asset-editor-mode" type="text" name="mode">'
    + '<option value="resize">'+ aapiT('resize') +'</option>'
    + '<option value="scale">'+ aapiT('scale') +'</option>'
    + '<option value="crop">'+ aapiT('crop') +'</option>'
    + '<option value="crop_and_scale">'+ aapiT('cropAndScale') +'</option>'
    + '</select></div>'
    
    + '<div class="form-item"><label for="asset-editor-nid">'+ aapiT('nodeId') +'</label>'
    + '<input id="asset-editor-nid" type="text" name="nid" /></div>'
    
    + '<div class="form-item"><label for="asset-editor-name">'+ aapiT('name') +'</label>'
    + '<input id="asset-editor-name" type="text" name="name" /></div>'
    
    + '<div class="form-item"><label for="asset-editor-id">'+ aapiT('htmlId') +'</label>'
    + '<input id="asset-editor-id" type="text" name="id" /></div>'
    
    + '<div class="form-item"><label for="asset-editor-style">'+ aapiT('style') +'</label>'
    + '<input id="asset-editor-style" type="text" name="style" /></div>'
    
    + '<input name="insert" value="'+ aapiT('use') +'" type="button" onclick="insertAsset();" id="tinyMCE-asset-insert"/>'
    + '</form>');
    
  $('#asset-editor-nid, #asset-editor-mode, #asset-editor-width, #asset-editor-height, #asset-editor-style').change(getPreview);
  $('#asset-editor label').css('display', 'block');
}

function getPreview(obj) {
  $('#asset-view').html('<div class="messages status">'+ aapiT('loading') +'</div>');
  
  var args = new Array();
  var nid = false;
  if(typeof(object) == 'object') {
    if(obj.width)   args.push('a[width]='+ obj.width);
    if(obj.height)  args.push('a[height]='+ obj.height);
    if(obj.mode)    args.push('a[mode]='+ obj.mode);
    if(obj.style)   args.push('a[style]='+ obj.style);
    if (obj.nid) {
      args.push('a[nid]=' + obj.nid);
      nid = obj.nid;
    }
  }
  else {
    var f = document.getElementById('asset-editor');
    if(f.width && f.width.value)   args.push('a[width]='+ f.width.value);
    if(f.height && f.height.value)  args.push('a[height]='+ f.height.value);
    if(f.mode && f.mode.value)    args.push('a[mode]='+ f.mode.value);
    if(f.style && f.style.value)   args.push('a[style]='+ f.style.value);
    if (f.nid && f.nid.value) {
      nid = f.nid.value;
      args.push('a[nid]=' + f.nid.value);
    }
  }
  
  
  if (nid) {
    $('#asset-view').html('<img src="'+aapiUrl('asset-view/n-'+nid+'-f-1', '', null)+'" alt="preview"/>');
    var url = aapiUrl(
      'asset-ajax/info/'+nid,
      args.join('&'),
      null
    );

    $.ajax({
      type: "GET",
      url: url,
      dataType: "json",
      success: function(msg){
        if (msg != null && msg.html_inclusion != null) {
          //$('#asset-view').html(msg.html_inclusion);
          
          if(!$('#asset-editor-name').val()) $('#asset-editor-name').val(msg.title);
          if(!$('#asset-editor-width').val() && msg.width) $('#asset-editor-width').val(msg.width);
          if(!$('#asset-editor-height').val() && msg.height) $('#asset-editor-height').val(msg.height);
        }
        else {
          $('#asset-view').html('<div class="messages error">'+ aapiT('error') +'</div>');
        }
      }
    });
  }
  
  
}

function insertAsset() {
	var fe, f = document.getElementById('asset-editor'), h;

	if (!AutoValidator.validate(f)) {
		alert(tinyMCE.getLang('lang_invalid_data'));
		return false;
	}

  if (!$('#asset-editor-mode').val()) {
    $('#asset-editor-mode').val('scale');
  }
  
  if (!$('#asset-editor-nid').val()) {
    alert('Node id is not defined');
    return false;
  }
  
	f.width.value = f.width.value == "" ? 100 : f.width.value;
	f.height.value = f.height.value == "" ? 100 : f.height.value;

	fe = tinyMCE.selectedInstance.getFocusElement();
	
  if (fe != null && /mceItemAsset/.test(tinyMCE.getAttrib(fe, 'class'))) {
	  $(fe)
      .attr('mode', f.mode.value)
      .attr('nid', f.nid.value)
      .attr('width', f.width.value)
      .attr('height', f.height.value)
      .attr('style', f.style.value)
      .attr('title', serializeParameters());
	}
  else {
    h = '<img src="' + tinyMCE.getParam("theme_href") + '/images/spacer.gif"';
    h += ' class="mceItemAsset asset"';
    
    h += ' mode="' + f.mode.value + '"';
    h += ' nid="' + f.nid.value + '"';
    
    h += ' width="' + f.width.value + '"';
    h += ' height="' + f.height.value + '"';
    h += ' style="' + f.style.value + '"';
    
    h += ' title="' + serializeParameters() + '"';
    h += ' />';
      
    tinyMCE.selectedInstance.execCommand('mceInsertContent', false, h);
	}
  tinyMCE.closeWindow(window);
  //tinyMCEPopup.close();
}

function getAssetListHTML() {
	return "";
}

function getType(v) {
	var fo, i, c, el, x, f = document.getElementById('asset-editor');

	fo = tinyMCE.getParam("asset_types", "flash=swf;shockwave=dcr;qt=mov,qt,mpg,mp3,mp4,mpeg;shockwave=dcr;wmp=avi,wmv,wm,asf,asx,wmx,wvx;rmp=rm,ra,ram").split(';');
	for (i=0; i<fo.length; i++) {
		c = fo[i].split('=');

		el = c[1].split(',');
		for (x=0; x<el.length; x++)
		if (v.indexOf('.' + el[x]) != -1)
			return c[0];
	}

	return null;
}

function switchType(v) {
	var t = getType(v), d = document, f = document.getElementById('asset-editor');

	if (!t)
		return;

	selectByValue(d.forms[0], 'asset_type', t);
	changedType(t);

	// Update qtsrc also
	if (t == 'qt' && f.src.value.toLowerCase().indexOf('rtsp://') != -1) {
		alert(tinyMCE.getLang("lang_asset_qt_stream_warn"));

		if (f.qt_qtsrc.value == '')
			f.qt_qtsrc.value = f.src.value;
	}
}

function changedType(t) {

}

function serializeParameters() {
	var d = document, f = document.getElementById('asset-editor'), s = '';


  s += getStr(null, 'mode');
  s += getStr(null, 'nid');
  
  
	s += getStr(null, 'id');
	s += getStr(null, 'name');
	s += getStr(null, 'width');
  s += getStr(null, 'height');
  s += getStr(null, 'style');

	s = s.length > 0 ? s.substring(0, s.length - 1) : s;

	return s;
}

function setBool(pl, p, n) {
	if (typeof(pl[n]) == "undefined")
		return;

	document.getElementById('asset-editor').elements[p + "_" + n].checked = pl[n];
}

function setStr(pl, p, n) {
	var f = document.getElementById('asset-editor'), e = f.elements[(p != null ? p + "_" : '') + n];

	if (typeof(pl[n]) == "undefined")
		return;

	if (e.type == "text")
		e.value = pl[n];
	else
		selectByValue(f, (p != null ? p + "_" : '') + n, pl[n]);
}

function getBool(p, n, d, tv, fv) {
	var v = document.getElementById('asset-editor').elements[p + "_" + n].checked;

	tv = typeof(tv) == 'undefined' ? 'true' : "'" + jsEncode(tv) + "'";
	fv = typeof(fv) == 'undefined' ? 'false' : "'" + jsEncode(fv) + "'";

	return (v == d) ? '' : n + (v ? ':' + tv + ',' : ':' + fv + ',');
}

function getStr(p, n, d) {
	var e = document.getElementById('asset-editor').elements[(p != null ? p + "_" : "") + n];
	var v = e.type == "text" ? e.value : e.options[e.selectedIndex].value;

	return ((n == d || v == '') ? '' : n + ":'" + jsEncode(v) + "',");
}

function getInt(p, n, d) {
	var e = document.forms[0].elements[(p != null ? p + "_" : "") + n];
	var v = e.type == "text" ? e.value : e.options[e.selectedIndex].value;

	return ((n == d || v == '') ? '' : n + ":" + v.replace(/[^0-9]+/g, '') + ",");
}

function themeForm(el) {
  var table = $(el)
    .add('table tr td')
    .attr('cellpadding', '4')
    .attr('cellspacing', '0')[0];
  var row = $(table).add('tr')[0];
  var cell = $(row).add('td').html('TADA');
}

function jsEncode(s) {
	s = s.replace(new RegExp('\\\\', 'g'), '\\\\');
	s = s.replace(new RegExp('"', 'g'), '\\"');
	s = s.replace(new RegExp("'", 'g'), "\\'");

	return s;
}
