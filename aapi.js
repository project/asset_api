/**
 * I know, I know, it could be scripted in a much better (oop) way...
 * so if you're like a JS ninja, feel free to improve... 
 */
var AAPIassets = new Array();

function AAPIasset(obj) {
  
  this.filepath = '';
  this.filename = '';
  this.nid = 0;
  this.type = '';
  this.element = null;
  
}

function AAPIsearch(el) {
  this.element = el;
  this.builded = false;
  this.searching = false;
  this.searchParams = {};
  
  this.build = function() {
    var reference = this;
    var html = '';//$(this).html();
    var options = '<option value=""> -- </option>';
    var resultsHolder = '';
    var types = Drupal.settings.aapi.types;
    
    for(var i=0; i<types.length; i++){
      options += '<option value="'+types[i]+'">'+types[i].split('aapi_').join('')+'</option>';
    }
    
    
    //aapiDbg($('#asset-search-results'));
    if ($('#asset-search-results')) {
      
    }
    else {
      resultsHolder = '<div id="asset-search-results"></div>';
    }
    
    html += '<div id="asset-search-tools">'
      
      + '<div class="form-item"><label for="asset-search-file">'+ aapiT('filepath') +'</label>'
      + '<input type="text" id="asset-search-file" /></div>'
      
      + '<div class="form-item"><label for="asset-search-title">'+ aapiT('title') +'</label>'
      + '<input type="text" id="asset-search-title" /></div>'
      
      + '<div class="form-item"><label for="asset-search-type">'+ aapiT('type') +'</label>'
      + '<select id="asset-search-type">'
      + options
      + '</select></div>'
      
      //+ resultsHolder
      
      + '</div>';
    $(this.element).html(html);
    
    $('#asset-search-file, #asset-search-type, #asset-search-title')
//    .blur(function(){
//      reference.search();
//    })
    .change(function(){
      reference.search();
    });
    $('#asset-search-file, #asset-search-title').keyup(function(){
      reference.search();
    });
    
    this.builded = true;
    ////aapiDbg([this, html]);
  };
  
  this.search = function() {
    if (this.searching == false) {
      this.searching = true;
      var reference = this;
      
      $('#asset-search-results').html('loading');
      
      var args = Array('');
      var proceed = 0;
      
      var file = $('#asset-search-file').val();
      if (file && file!='') {
        args.push('file/'+file);
        if (this.searchParams.file != file) {
          proceed++;
        }
      }
      
      var title = $('#asset-search-title').val();
      if (title && title!='') {
        args.push('title/'+title);
        if (this.searchParams.title != title) {
          proceed++;
        }
      }
      
      var type = $('#asset-search-type').val();
      if (type && type!='') {
        args.push('type/'+type);
        if (this.searchParams.type != type) {
          proceed++;
        }
      }
      
      this.searchParams.file = file;
      this.searchParams.title = title;
      this.searchParams.type = type;
      
      if (proceed >= 1) {
          
        $.ajax({
          type: "POST",
          url: aapiUrl('asset-ajax/search'+args.join('/')),
          dataType: "json",
          success: function(data){
            if (data != null) {
              $('#asset-search-results').html('');
              $.map(data, function(a){
                return reference.buildAsset(a);
              });
            }
            reference.searching = false;
          },
          error: function() {
            reference.searching = false;
          }
        });
        
      }
      else {
        this.searching = false;
      }
    }
  };
  
  this.buildAsset = function(a) {
    return aapi_create_html_result(a);
    
  };
  
  this.build();
}

function AAPI(obj) {
  
  this.asset = new AAPIasset({
    'filepath'  :'',
    'filename'  :'',
    'nid'       :0,
    'type'      :false,
    'el'        :false
  });
  
  this.cssIntVal = function(el, f) {
    return Number(el.css(f).replace(/px/,''));
  };
  
  this.hasClass = function(el, c) {
    var a = el.attr('class');
    if (a.length==0) {
      return false;
    }
    a = a.split(' ');
    for (var i=0; i<a.length; i++) {
      if (a[i]==c) {
        return true;
      }
    }
    return false;
  };
  
  this.thumbType = function(el) {
    var types = Drupal.settings.aapi.types;
    for (var i=0; i<types.length; i++) {
      if (aapi_has_class(el, types[i])) {
        return types[i];
      }
    }
    return false;
  };
  
  this.inclusionField = function(info) {
/*
    var toolbox = $('#asset-browser-tools');
    var html = '<input type="text" id="asset-inclusion" value="[asset-'+asset.nid+']"/>';
    if (window.opener && window.opener.assetField) {
      var action = '$(\'#asset-inclusion\').val()';
      if (window.opener.assetAction) {
        action = window.opener.assetAction;
      }
      html += ' <input type="button" value="'+aapiT('use')+'" onclick="window.opener.aapi_apply('+action+');window.close();"/>';
    }
    toolbox.html(html);
    
    info += '<div class="asset-info asset-title"><span class="label">'+aapiT('title')+':</span> <a class="info" href="'+aapiUrl('node/'+asset.nid)+'">'+asset.title+'</a> <a href="'+aapiUrl('node/'+asset.nid+'/delete', 'destination=asset-browser')+'">['+aapiT('delete')+']</a></div>';
    
    info += '<div class="asset-info asset-filepath"><span class="label">'+aapiT('filepath')+':</span> <span class="info"><a href="'+aapiUrl(asset.filepath, null, null, true)+'">'+asset.filepath+'</a></span></div>';
    
    info += '<div class="asset-info asset-filesize"><span class="label">'+aapiT('size')+':</span> <span class="info">'+asset.filesize+'</span></div>';
    
    info += '<div class="asset-info asset-mime"><span class="label">'+aapiT('mime')+':</span> <span class="info">'+asset.filemime+'</span></div>';
    
    $('#asset-info').html(info);
*/
  };
}

var asset = {
  'filepath'  :'',
  'filename'  :'',
  'nid'       :0,
  'type'      :false,
  'el'        :false
};
var Assets = Array();

function aapi_value(el, f){
  return Number(el.css(f).replace(/px/,''));
}

function aapi_has_class(el, c){
  var a = el.attr('class');
  if (a.length==0) {
    return false;
  }
  a = a.split(' ');
  for (var i=0; i<a.length; i++) {
    if (a[i]==c) {
      return true;
    }
  }
  return false;
}

function aapi_thumb_type(el){
  var types = Drupal.settings.aapi.types;
  for (var i=0; i<types.length; i++) {
    if (aapi_has_class(el, types[i])) {
      return types[i];
    }
  }
  return false;
}

function aapi_inclusion_field(info){
  if ($('#asset-editor-nid')) {
    aapiDbg(asset);
    $('#asset-editor-nid').val(asset.nid);
    $('#asset-editor-name').val(asset.title);
    $('#asset-editor-width').val('');
    $('#asset-editor-height').val('');
    if (asset.width) {
      $('#asset-editor-width').val(asset.width);
    }
    if (asset.height) {
      $('#asset-editor-height').val(asset.height);
    }
    if (typeof(getPreview) == 'function') {
      getPreview();
    }
  }
}

var aapiSearch;

function aapi_init(){
  //browser
  if (document.getElementById('asset-search-results')) {
    $('#asset-search-results').html('');
  }
  aapiSearch = new AAPIsearch(document.getElementById('asset-search-form'));
  
  //quick upload
  if (document.getElementById('aapi-quick-upload')) {
    //aapiDbg($('#aapi-quick-upload'));
  }
  
  if (typeof(window.init) == 'function') {
    window.init();
  }
}

function aapi_search_request(){
  var args = Array('');
  
  var file = $('#asset-search-file').val();
  if (file && file!='') {
    args[args.length] = 'file/'+file;
  }
  
  var title = $('#asset-search-title').val();
  if (title && title!='') {
    args[args.length] = 'title/'+title;
  }
  
  var type = $('#asset-search-type').val();
  if (type && type!='') {
    args[args.length] = 'type/'+type;
  }
  
  $('#asset-search-results').html('loading');
  
  $.ajax({
    type: "POST",
    url: aapiUrl('asset-ajax')+"/search"+args.join('/'),
    dataType: "json",
    success: function(data){
      window.assetAjaxResults = data;
      $('#asset-search-results').html('');
      $.map(data, aapi_create_html_result);
    }
  });
}

function aapi_create_html_result(a){
  Assets[Number(a.nid)] = a;
  AAPIassets[Number(a.nid)] = a;
  var holder = $('#asset-search-results');
  var id = 'asset-'+a.nid;
  var icon = '';
  if (a.icon) {
    icon = a.icon;
  } else {
    icon = '<img src="'+aapiUrl('asset-view')+'/n-'+a.nid+'-m-s-w-95-h-95" alt="'+a.filepath+'"/>';
  }
  var el = '<div id="'+id+'" class="search-result '+a.type+'" title="'+a.filepath+'">'+icon+'<br/>'+a.title+'</div>';
  holder.append(el);
  $('#'+id).bind('click', function(){
    asset = Assets[Number(a.nid)];
    aapi_invoke(asset.type+'_browser_thumb');
  });
}

function aapi_invoke(funcname){
  if ('function' == typeof eval('window.'+funcname)) {
    eval(funcname+'();');
  }
}

function aapi_apply(value){
  $('#'+window.assetField).val(value);
}

function aapiUrl(path, query, fragment, file, absolute) {
    //console.info(arguments, Drupal.settings.aapi);
    query = query ? query : "";
    fragment = fragment ? "#" + fragment : "";
    
    var joint = '';
    var queryJoint = '?';
    if(!Drupal.settings.aapi.cleanurls || file!=true){
      joint += '?q=';
      queryJoint = '&';
    }
    
    var base = window.location.pathname;
    if (absolute === true) {
      base = window.location.protocol +'//'+ window.location.host + base; 
    }
    
    if (query) {
        return base + joint + path + queryJoint + query + fragment;
    } else {
        return base + joint + path + fragment;
    }
}

function aapiDbg(data){
  if (window.console && window.console.info) {
    console.info(data);
  }
}

function aapiT(item) {
  return eval('Drupal.settings.aapi.i18n.'+item);
}


$(document).ready(aapi_init);