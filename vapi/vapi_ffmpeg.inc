<?php

/**
 * Process encoding
 *
 * @param string $filepath
 * @param string $destination
 * @return boolean
 */

function vapi_ffmpeg_encoding($filepath, $destination) {
  $search = array(
    '[filepath]',
    '[destination]'
  );
  $replace = array(
    escapeshellarg(str_replace(file_directory_path(), variable_get('vapi_absolute_path', ''), $filepath)),
    escapeshellarg(str_replace(file_directory_path(), variable_get('vapi_absolute_path', ''), $destination)),
  );
  $cmd = 'nice -n 10 '. str_replace($search, $replace, variable_get('vapi_encoding_cmd', 'ffmpeg -i [filepath] -y -f flv -b 1024k -an [destination] -acodec libmp3lame -ar 44100 -ab 128k -newaudio'));
  
  $vstats = base_path() .'vstats_'. date('His') .'.log';
  
  $exec = exec($cmd, $output, $return);
  
  
  if ($return !== FALSE && is_file($destination) && filesize($destination)) {
    return TRUE;
  }
  
  
  return FALSE;
}

/**
 * Adds meta data to an FLV file
 *
 * @param string $filepath
 * @return boolean (at least, i guess)
 */

function vapi_ffmpeg_meta($filepath) {
  
  $search = array(
    '[filepath]'
  );
  $replace = array(
    escapeshellarg(str_replace(file_directory_path(), variable_get('vapi_absolute_path', ''), $filepath)),
  );
  $cmd = 'nice -n 10 '. str_replace($search, $replace, variable_get('vapi_meta_cmd', 'flvtool2 -U [filepath]'));
  
  $exec = exec($cmd, $output, $return);
  
  return $return;
}

/**
 * Utility function
 *
 * @param array $array
 * @return array
 */

function _vapi_ffmpeg_clean_array($array) {
  foreach ($array as $k => $v) {
    if (empty($v)) {
      unset($array[$k]);
    }
  }
  return $array;
}

/**
 * List related running processes
 *
 * @return array containing headers and rows for a theme('table', $returned_array) use
 */
function vapi_ffmpeg_processes() {
  $cmd = 'ps -C ffmpeg u && ps -C flvtool2 u --no-heading';
  exec($cmd, $processes);
  $rows = array();

  $h = explode(" ", array_shift($processes));
  $headers = array();
  foreach ($h as $n => $column) {
    if ($column == '') {
      //unset($headers[$n]);
    }
    else {
      $headers[] = $column;
    }
  }

  $columns = count($headers);
  foreach ($processes as $line) {
    $l = explode(" ", $line);
    $line = array();
    foreach ($l as $c => $v) {
      if (empty($v)) {
        //unset($line[$c]);
      }
      else {
        $line[] = $v;
      }
    }
    $rows[] = $line;
  }

  return array($rows, $headers);
}

/**
 * Extract informations about a video file
 *
 * @param string $filepath
 * @return object
 */
function vapi_ffmpeg_info($filepath) {
  
  $search = array(
    '[filepath]',
  );
  $replace = array(
    escapeshellarg(realpath($filepath)),//escapeshellarg(str_replace(file_directory_path(), variable_get('vapi_absolute_path', ''), $filepath)),
  );
  $cmd = 'nice -n 10 '. str_replace($search, $replace, variable_get('vapi_info_cmd', 'ffmpeg -i [filepath] 2>&1'));
  
  $exec = exec($cmd, $output, $return);
  
  $info = array(
    'duration' => NULL,
    'width' => NULL,
    'height' => NULL,
    'video' => array(),
    'audio' => array(),
  );
  foreach ($output as $k => $line) {
    $line = _vapi_ffmpeg_clean_array(explode(' ', trim($line)));
    $line_head = array_shift($line);
    if (strtolower($line_head) == 'stream') {
      $stream_num = array_shift($line);
      $stream_type = array_shift($line);
      
      $info[substr(strtolower($stream_type), 0, -1)][] = explode(',', str_replace(implode(' ', array($line_head, $stream_num, $stream_type)), '', trim($output[$k])));
    }
    elseif (strtolower($line_head) == 'duration:') {
      $duration = explode(':', substr($line[0], 0, -1));
      $info['duration'] = intval($duration[0]) * (60*60);
      $info['duration'] += intval($duration[1]) * 60;
      $info['duration'] += floatval($duration[2]);
    }
  }
  
  $dim = explode(' ', trim($info['video'][0][2]));
  list($width, $height) = explode('x', $dim[0]);
  $info['width'] = intval($width);
  $info['height'] = intval($height);
  $info['diagonal'] = sqrt(pow($width, 2)+pow($height, 2));
  
  return (object)$info;
}

