<?php

define('VAPI_TO_ENCODE', 0);
define('VAPI_ENCODING', 1);
define('VAPI_ENCODED', 2);
define('VAPI_ENCODING_ERROR', 3);

function vapi_perm() {
  return array(
    'administer vapi',
  );
}

function vapi_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/vapi',
      'title' => t('Video API'),
      'description' => t('Administer video API settings'),
      'access' => user_access('administer video API'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('vapi_settings'),
      'type' => MENU_NORMAL_ITEM,
    );
    $items[] = array(
      'path' => 'admin/content/vapi',
      'title' => t('Video API'),
      'description' => t('Overview of the video API works'),
      'access' => user_access('administer video API'),
      'callback' => 'theme',
      'callback arguments' => array('vapi_queue'),
      'type' => MENU_NORMAL_ITEM,
    );
    $items[] = array(
      'path' => 'admin/content/vapi/info',
      'title' => t('Video API'),
      'description' => t('Overview of the video API works'),
      'access' => user_access('administer video API'),
      'callback' => 'theme',
      'callback arguments' => array('vapi_queue'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => 0,
    );
    $items[] = array(
      'path' => 'admin/content/vapi/import',
      'title' => t('Import'),
      'description' => t('Import bulk video'),
      'access' => user_access('administer video API'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('vapi_import'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 1,
    );
    $items[] = array(
      'path' => 'admin/content/vapi/logs',
      'title' => t('Logs'),
      'description' => t('Logs'),
      'access' => user_access('administer video API'),
      'callback' => 'vapi_logs',
      'type' => MENU_LOCAL_TASK,
      'weight' => 2,
    );
    $items[] = array(
      'path' => 'admin/content/vapi/clean',
      'title' => t('Clean'),
      'description' => t('Clean'),
      'access' => user_access('administer video API'),
      'callback' => 'vapi_list_the_shit',
      'type' => MENU_LOCAL_TASK,
      'weight' => 3,
    );
  }
  return $items;
}

function vapi_settings() {
  $form = array();
  
  $form['vapi_encoding_cmd'] = array(
    '#type' => 'textfield',
    '#title' => t('Encoding command'),
    '#default_value' => variable_get('vapi_encoding_cmd', 'ffmpeg -i [filepath] -y -f flv -b 1024k -an [destination] -acodec libmp3lame -ar 44100 -ab 128k -newaudio'),
  );
  
  $form['vapi_meta_cmd'] = array(
    '#type' => 'textfield',
    '#title' => t('Metas creation command'),
    '#default_value' => variable_get('vapi_meta_cmd', 'flvtool2 -U [destination]'),
  );
  
  $form['vapi_enable_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable VAPI cron'),
    '#default_value' => variable_get('vapi_enable_cron', 0),
  );
  
  return system_settings_form($form);
}

/**
 * Check if the extension (of a file) is supported
 */
function vapi_ext_support($filename) {
  $ext = strtolower(array_pop(explode('.', $filename)));
  $convertible = explode(' ', variable_get('vapi_convertible', 'wmv avi mpg'));
  if (in_array($ext, $convertible)) {
    //drupal_set_message($filename);
    return TRUE;
  }
  return FALSE;
}

/**
 * Correct the file path to replace the file extension with "flv"
 */
function vapi_flv_path($filepath) {
  $basename = basename($filepath);
  $ext = array_pop(explode('.', $filepath));
  $newpath = dirname($filepath) .'/'. basename($filepath, $ext) .'flv';
  //drupal_set_message($newpath);
  
  return $newpath;
  //return str_replace(file_directory_path(), variable_get('vapi_absolute_path', ''), $newpath);
}

/**
 * Lists convertible files
 */
function vapi_importables() {
  static $results = array();
  if (count($results)>0) {
    return $results;
  }


  $flat_files = file_scan_directory(file_directory_path(), '.*', array('.', '..', 'CVS'));
  $req = db_query("SELECT filepath FROM {files}");
  $db_files = array();
  while ($res = db_fetch_object($req)) {
    $db_files[] = $res->filepath;
  }

  $req = db_query("SELECT filepath FROM {vapi_import}");
  $db_import = array();
  while ($res = db_fetch_object($req)) {
    $db_import[] = $res->filepath;
  }

  foreach ($flat_files as $path => $file) {
    if (!in_array(vapi_flv_path($path), $db_files) && !in_array($path, $db_import) && vapi_ext_support($path)) {
      $results[md5($path)] = $path;
    }
  }

  return $results;
}

/**
 * Form that lists convertible files
 */
function vapi_import() {
  
  $req = db_query('SELECT filepath, md5 FROM {vapi_import} WHERE status=%s', VAPI_ENCODING);
  
  while ($fp = db_fetch_object($req)) {
    $fs = intval(@filesize(vapi_flv_path($fp->filepath)));
    if ($fs > 0) {
      $res = vapi_set_status($fp->filepath, VAPI_ENCODED);
    }
    else {
      $res = vapi_set_status($fp->filepath, VAPI_ENCODING_ERROR);
    }
    //drupal_set_message('reset "'. $fs .'" "'. intval($res) .'": '. $fp->filepath);
  }
  
  $form = array();

  $form['info'] = array(
    '#value' => theme('vapi_process'),
  );

  $form['vapifiles'] = array(
    '#type' => 'checkboxes',
    '#options' => vapi_importables(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  $form['reset_import'] = array(
    '#type' => 'submit',
    '#value' => t('Reset encoding'),
  );

  return $form;
}

/**
 * Control the aspect of the import page
 */
function theme_vapi_import($form) {
  $headers = array(theme('table_select_header_cell'), t('Filepath'));
  $rows = array();


  foreach (element_children($form['vapifiles']) as $id) {
    $title = str_replace(file_directory_path(), '', $form['vapifiles'][$id]['#title']);
    unset($form['vapifiles'][$id]['#title']);
    $rows[] = array(
      drupal_render($form['vapifiles'][$id]),
      $title
    );
  }

  $form['vapifiles'] = array(
    '#value' => theme('table', $headers, $rows),
  );
  
  return drupal_render($form);
}

/**
 * Import the select files
 */
function vapi_import_submit($form_id, $form_values) {
  if ($form_values['op'] == $form_values['reset_import']) {
    db_query('UPDATE {vapi_import} SET status=0 WHERE status=1');
  }
  else {
    $importables = vapi_importables();
    $count = 0;

    foreach ($form_values['vapifiles'] as $md5) {
      //drupal_set_message($md5);
      if (!empty($md5)) {
        $query = intval(db_query("INSERT INTO {vapi_import} (timestamp, filepath, md5) VALUES (UNIX_TIMESTAMP(), '%s', '%s')", $importables[$md5], $md5));
        
        //drupal_set_message($md5 .' "'. $query .'"');
        
        if ($query != 0) {
          $count++;
        }
      }
    }
    drupal_set_message($count .' '. t('files will be imported'));
  }
}

function vapi_cron() {
  if (variable_get('vapi_enable_cron', 0)) {
    if (!vapi_check_encoding()) {
      //db_query('UPDATE {vapi_import} SET status=%d WHERE status=%d', VAPI_ENCODING_ERROR, VAPI_ENCODING);
      //print ' no job';
      $next = db_result(db_query("SELECT filepath FROM {vapi_import} WHERE status=%d ORDER BY LENGTH(filepath) DESC LIMIT 1", VAPI_TO_ENCODE));
  
      if (!empty($next)) {
        vapi_encode($next, $_GET['f']);
      }
      /*else {
        watchdog('vapi', t('Next is empty'));
      }*/
    }
    else {
      //print ' working';
    }
    flush();
  }
  else {
    //print t('VAPI cron blocked');
  }
}

function vapi_check_encoding() {
  $ps = vapi_invoke('processes');//vapi_search_process();
  
  if (count($ps[0]) == 0) {
    return FALSE;
  }
  return TRUE;
  
}

function vapi_encode($filepath, $repeat=FALSE) {
  static $tt;
  static $tries = 0;
  $destination = vapi_flv_path($filepath);
  
  if ($tt == NULL) {
    $tt = time();
  }
  
  $wd = array(
    'filepath' => $filepath,
    'destination' => $destination,
  );
  
  flush();
  
  if ($tries < 20) {
    $wd['tries'] = $tries;
    $tries++;
    
    if (!is_file($destination)) {
      
      
      vapi_set_status($filepath, VAPI_ENCODING);
      //print ' starting';
      
      flush();
      
      if (vapi_invoke('encoding', $filepath, $destination)) {
        
        if (function_exists('_aapi_node_import')) {
          _aapi_node_import($destination, aapi_r($wd), 1, 1);
        }
        vapi_set_status($filepath, VAPI_ENCODED);
        
        //print ' encoding ok';
        $wd['encoding'] = 'ok';
        
        flush();
        
        if (vapi_invoke('meta', $destination)) {
          //print ' meta ok';
          $wd['meta'] = 'ok';
        }
        else {
          //print ' meta error';
          $wd['meta'] = 'error';
        }
        
        //watchdog('vapi', aapi_r($wd));
        
        return TRUE;
        
      }
      
      //print ' encoding error';
      $wd['encoding'] = 'error';
      
      vapi_set_status($filepath, VAPI_ENCODING_ERROR);
      watchdog('vapi', aapi_r($wd));
      
      vapi_cron();
      return FALSE;
      
    }
    else {
      $wd['filesize'] = filesize($destination);
      //print $destination .' exists';
      
      vapi_set_status($filepath, VAPI_ENCODED);
      
      $exists = db_result(db_query("SELECT filepath FROM {files} WHERE filepath='%s'", $filepath));
      if (empty($exists) && function_exists('_aapi_node_import')) {
        _aapi_node_import($destination, aapi_r($wd), 1, 1);
      }
      
      watchdog('vapi', $destination .' '. t('exists'));
      vapi_cron();
    }
    
  }
  else {
    //print ' to many tries';
    return FALSE;
  }
  
  return FALSE;
}

function vapi_set_status($filepath, $status) {
  $req = db_query("UPDATE {vapi_import} SET status=%d WHERE md5='%s'", $status, md5($filepath));
  if (!$req) {
    watchdog('vapi', t('Can not set status (!status) for !filepath.', array(
      '!filepath' => $filepath,
      '!status' => $status,
    )));
  }
  return $req;
}

function vapi_queue_stats() {
  $results = array();
  $req = db_query('SELECT status, count(*) AS count FROM `vapi_import` GROUP BY status ASC');

  while ($res = db_fetch_object($req)) {
    $results[$res->status] = $res->count;
  }

  return $results;
}

function theme_vapi_queue_stats() {
  $stats = vapi_queue_stats();

  $headers = array(
    t('Status'),
    t('Count'),
  );
  $rows = array();

  foreach ($stats as $status => $count) {
    $rows[] = array(
      vapi_queue_labels($status),
      $count
    );
  }
  $files = theme('table', $headers, $rows);



  //$ps = vapi_search_process();
  //$process = theme('table', $ps[1], $ps[0]);

  return $files;//.$process;
}

function theme_vapi_info($info) {
  $lines = array();
  
  $lines[] = t('Duration') .': '. format_interval($info->duration);
  $lines[] = t('Width') .': '. $info->width;
  $lines[] = t('Height') .': '. $info->height;
  $lines[] = t('Diagonal') .': '. sqrt(pow($info->height, 2)+pow($info->width, 2));
  
  $streams = array();
  foreach ($info->video as $stream) {
    $streams[] = implode(', ', $stream);
  }
  $lines[] = t('Video streams') .': '.theme('item_list', $streams);
  
  $streams = array();
  foreach ($info->audio as $stream) {
    $streams[] = implode(', ', $stream);
  }
  $lines[] = t('Audio streams') .': '. theme('item_list', $streams);
  
  return theme('item_list', $lines);
}

function theme_vapi_queue() {
  
  $headers = array(
    array('data' => t('Status'), 'field' => 'status', 'sort' => 'desc'),
    array('data' => t('File path'), 'field' => 'filepath'),
    array('data' => t('Timestamp'), 'field' => 'timestamp'),
  );
  
  $req = pager_query('SELECT status, filepath, timestamp FROM {vapi_import} WHERE status!=%d AND status!=%d'. tablesort_sql($headers), 10, 0, NULL, VAPI_ENCODED, VAPI_ENCODING_ERROR);
  
  
  if (!function_exists('vapi_ffmpeg_info')) {
    require_once(drupal_get_path('module', 'vapi') .'/vapi_ffmpeg.inc');
  }
  
  while ($res = db_fetch_object($req)) {
    $rows[] = array(
      vapi_queue_labels($res->status),
      $res->filepath .'<hr/>'. theme('vapi_info', vapi_ffmpeg_info($res->filepath)),
      format_date($res->timestamp),
    );
  }
  
  
  return theme('vapi_process') . theme('pager') . theme('table', $headers, $rows) . theme('pager');
}

function vapi_logs () {
  
  $logs = array(
    '#type' => 'fieldset',
    '#title' => t('Logs'),
    '#value' => vapi_watchdog_overview(),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  return theme('fieldset', $logs);
}

function theme_vapi_process() {
  $ps = vapi_invoke('processes');
  if (count($ps[0]) > 0) {
    
    foreach ($ps[0] as $k=>$c) {
      $line = array_slice($ps[0][$k], 0, count($ps[1])-1);
      $line[] = '<code>'. implode(' ', array_slice($ps[0][$k], count($ps[1])-1)) .'</code>';
      $ps[0][$k] = $line;
    }
    
    return theme('table', $ps[1], $ps[0]);// . aapi_r($ps);
  }
  else {
    return t('No encoding running process');
  }
}

function vapi_queue_labels($status=NULL) {
  $labels = array(
    VAPI_TO_ENCODE => t('to be encoded'),
    VAPI_ENCODING => t('encoding'),
    VAPI_ENCODED => t('encoded'),
    VAPI_ENCODING_ERROR => t('encoding fails'),
  );

  if ($status!==NULL) {
    return $labels[intval($status)];
  }

  return $labels;
}

function vapi_watchdog_overview() {
  $icons = array(WATCHDOG_NOTICE  => '',
                 WATCHDOG_WARNING => theme('aapi_thumbnail', 'misc/watchdog-warning.png', t('warning'), t('warning')),
                 WATCHDOG_ERROR   => theme('aapi_thumbnail', 'misc/watchdog-error.png', t('error'), t('error')));
  $classes = array(WATCHDOG_NOTICE => 'watchdog-notice', WATCHDOG_WARNING => 'watchdog-warning', WATCHDOG_ERROR => 'watchdog-error');

  $header = array(
    ' ',
    array('data' => t('Type'), 'field' => 'w.type'),
    array('data' => t('Date'), 'field' => 'w.wid', 'sort' => 'desc'),
    array('data' => t('Message'), 'field' => 'w.message'),
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('Operations'))
  );

  $sql = "SELECT w.wid, w.uid, w.severity, w.type, w.timestamp, w.message, w.link, u.name FROM {watchdog} w INNER JOIN {users} u ON w.uid = u.uid";
  $tablesort = tablesort_sql($header);
  $type = $_SESSION['watchdog_overview_filter'];
  $result = pager_query($sql ." WHERE w.type = '%s'". $tablesort, 50, 0, NULL, 'vapi');

  while ($watchdog = db_fetch_object($result)) {
    $rows[] = array('data' =>
      array(
        // Cells
        $icons[$watchdog->severity],
        t($watchdog->type),
        format_date($watchdog->timestamp, 'small'),
        l(truncate_utf8($watchdog->message, 56, TRUE, TRUE), 'admin/logs/event/'. $watchdog->wid, array(), NULL, NULL, FALSE, TRUE),
        theme('username', $watchdog),
        $watchdog->link,
      ),
      // Attributes for tr
      'class' => "watchdog-". preg_replace('/[^a-z]/i', '-', $watchdog->type) .' '. $classes[$watchdog->severity]
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No log messages available.'), 'colspan' => 6));
  }

  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, 50, 0);

  return $output;
}



function vapi_clean_the_shit($filepath) {
  static $results = array();
  
  if ($filepath == NULL) {
    return $results;
  }
  
  if (filesize($filepath) == 0) {
    file_delete($filepath);
    $results[] = $filepath;
  }
}

function vapi_list_the_shit() {
  $results = file_scan_directory(file_directory_path(), '.*', array('.', '..', 'CVS'), 'vapi_clean_the_shit', TRUE);
  
  return aapi_r(vapi_clean_the_shit(NULL));
}

/**
 * Invoke the VAPI toolkit function, the toolkits should provide:
 * - encoding, with $filepath, $destination
 * - meta, with $filepath
 * - info, with $filepath
 * - processes
 */
function vapi_invoke() {
  static $loaded = FALSE;
  $args = func_get_args();
  $toolkit = variable_get('vapi_toolkit', 'ffmpeg');
  $function = 'vapi_'. $toolkit .'_'. array_shift($args);
  
  if ($loaded!==TRUE) {
    require_once(drupal_get_path('module', 'vapi') .'/vapi_'. $toolkit .'.inc');
    $loaded = TRUE;
  }
  
  if (function_exists($function)) {
    return call_user_func_array($function, $args);
  }
  
  watchdog('vapi', t('Can not invoke !function.', array('!function' => $function)));
}


