README (or blame yourself) file for Asset API module

1. Presentation (of the module)
2. Installation
3. Hacking
4. Todo

1. Presentation ----------------------------------------------------------------
Well... i guess you now why you download this Drupal module...
Anyway, it has for purpose to ease the management/gestion of assets with Drupal.
The main idea comes from the DrupalCon07 (Barcelona) of the Asset manager
module. I won't explain why in mind it doesn't I thought it was a nightmare, the
people who have try to install it perhaps know what I'm talking about (at least,
I guess and hope so).

2. Installation ----------------------------------------------------------------
As usual, extract, move to the modules folder, activate them in the admin panel
and finally take a look at the settings.

Important note: the modules as they are at the moment delivered are not
complete because i'm not sure to be allowed to publish some files under Drupal
licence... :( (i'm not a lawyer) but i'll try to figure out which scripts i'm
allowed to publish...
You may find additional informations in the DEPENDENCIES.txt files

3. Hacking ---------------------------------------------------------------------
Adding teaser vs body filter behavior:
in node.module:
lines 727 to 732 replace with:

  if ($teaser == FALSE) {
    $node->body = check_markup($node->body, $node->format, FALSE, $teaser);
  }
  else {
    $node->teaser = check_markup($node->teaser, $node->format, FALSE, $teaser);
  }

  Then in filter.module
lines 746 replace with:

function check_markup($text, $format = FILTER_FORMAT_DEFAULT, $check = TRUE, $teaser = FALSE) {

on line 769:

      $text = module_invoke($filter->module, 'filter', 'prepare', $filter->delta, $format, $text, $teaser);

and finally, line 774:

      $text = module_invoke($filter->module, 'filter', 'process', $filter->delta, $format, $text, $teaser);

That's all folks!

4. Todo ------------------------------------------------------------------------
- clean the code
- improve documentation
- complete TinyMCE plugin