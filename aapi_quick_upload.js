var aquReady = false;

function aapi_quick_upload_init(){
  if (window.console == undefined || window.console.info == undefined) {
    window.console = {};
    window.console.info = function(){
    };
  }
  
  if ($('#aapi-quick-upload-form')) {
    $('#aapi-quick-upload-form').parent().append('<div id="aapi-quick-upload-loader">loading</div>');
  
    var src = ' src="' + Drupal.settings.aapi_quick_upload.callback + '"';
    var target = 'aapiquickuploadframe';
    var width = 'width:1px;';
    var height = 'height:1px;';

    if (Drupal.settings.aapi_quick_upload.mode == 'js') {
      $('#aapi-quick-upload-form').attr('action', Drupal.settings.aapi_quick_upload.callback);
      $('#aapi-quick-upload-form').attr('target', target);
      $('body').append('<div id="aapi-quick-upload-form-target-holder">holder</div>');
      src = '';
      
      $('#aapi-quick-upload-form .form-submit').click(function(){
        $('#aapi-quick-upload-form').hide();
        $('#aapi-quick-upload-loader').show();
        
        this.form.submit();
      });
    }
    else {
      width = 'width:' + $('#aapi-quick-upload-form').width() + 'px;';
      height = 'height:' + ($('#aapi-quick-upload-form').height() + 8) + 'px;';
      $('#aapi-quick-upload-form').parent().html('<div id="aapi-quick-upload-form-target-holder">holder</div>');
    }
    
    $('#aapi-quick-upload-form-target-holder').before('<iframe' + src + ' id="aapi-quick-upload-form-frame" name="' + target + '" style="' + width + '' + height + 'margin:0;padding:0;border:none;"></iframe>');
    $('#aapi-quick-upload-form-target-holder')
      .before('<div id="aapi-quick-upload-messages"></div>')
      .hide();
    $('#aapi-quick-upload-messages').hide();
    
  }
}

function aquClose() {
  $('#aapi-quick-upload-form').show();
  $('#aapi-quick-upload-form-loader').hide();
  $('#aapi-quick-upload-form-target-holder').hide();
  $('#aapi-quick-upload-messages').html('').hide();
}

function aapi_quick_upload_uploaded(uploadedAssets){
  $('#aapi-quick-upload-loader').hide();
  
  var i18n = Drupal.settings.aapi_quick_upload.i18n;
  if (console && console.info) {
    console.info(uploadedAssets);
  }
  
  
  if (uploadedAssets != null && uploadedAssets != undefined) {
    if (uploadedAssets.nodes.length > 1) {
      $('.form-file').val('');
      
      var str = '<a href="javascript:aquClose();void(0);">[hide]</a><ul>';
      for (var i = 0; i < uploadedAssets.nodes.length; i++) {
        str += '<li>' + uploadedAssets.nodes[i].inclusion + '<strong>' + i18n.editMore + ':</strong> <a href="' + uploadedAssets.nodes[i].edit + '">' + uploadedAssets.nodes[i].title + '</a></li>';
      }
      str += '</ul>';
      
      $('#aapi-quick-upload-messages')
        .html(str)
        .show();
      $('#aapi-quick-upload-form-target-holder')
        .css('position', 'fixed')
        .css('width', window.screen.availWidth + 'px')
        .css('height', window.screen.availHeight + 'px')
        .css('left', '0px')
        .css('top', '0px')
        .fadeTo("slow", 0.5);
      
    }
    else {
      if (uploadedAssets.nodes.length == 1) {
        if (Drupal.settings.aapi_quick_upload.askConfirmation && confirm(i18n.editOne)) {
          window.location.href = uploadedAssets.nodes[0].edit;
        }
        else if (typeof(window.getPreview) == 'function' && document.getElementById('asset-editor')) {
          var n = uploadedAssets.nodes[0];
          console.info(n);
          var f = document.getElementById('asset-editor');
          f.width.value = n.node.width;
          f.height.value = n.node.height;
          f.nid.value = n.nid;
          getPreview();
        }
      }
      else {
        alert(i18n.uploadError);
      }
      $('#aapi-quick-upload-form').fadeIn();
    }
  }
}
$(document).ready(function(){
  aapi_quick_upload_init();
});
